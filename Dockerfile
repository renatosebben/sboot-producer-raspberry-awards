FROM maven:3-jdk-11
WORKDIR /tmp/
COPY pom.xml /tmp/
COPY src /tmp/src/
RUN mvn clean package -DskipTests
EXPOSE 5000
CMD java -jar target/sboot-producer-raspberry-awards-0.0.1-SNAPSHOT.jar
