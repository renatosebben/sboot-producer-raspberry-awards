# sboot-producer-raspberry-awards

## Iniciando...

- `git clone git@gitlab.com:renatosebben/sboot-producer-raspberry-awards.git`
- `cd sboot-producer-raspberry-awards`

Agora você poderá executar os vários comandos abaixo.

## Pré-requisitos

- `mvn --version`<br>
  você deverá ver a indicação da versão do Maven instalada e
  a versão do JDK, dentre outras. Observe que o JDK é obrigatório, assim como
  a definição das variáveis de ambiente **JAVA_HOME** e **M2_HOME**.

## Limpar, compilar, executar testes de unidade e cobertura

- `mvn clean`<br>
  remove diretório _target_

- `mvn compile`<br>
  compila o projeto, deposita resultados no diretório _target_
  
## Empacotando o projeto

- `mvn package`<br>
  gera arquivo \sboot-producer-raspberry-awards-0.0.1-SNAPSHOT.jar no diretório _target_. Observe que
  o arquivo gerado não é executável. Um arquivo jar é um arquivo no formato
  zip. Você pode verificar o conteúde deste arquivo ao executar o comando `java jar sboot-producer-raspberry-awards-0.0.1-SNAPSHOT.jar`.

## Executando a aplicação e a RESTFul API

- `mvn exec:java -Dexec.mainClass="com.sebben.sboot.SbootApplication"`<br>
  executa o método _main_ da classe indicada na configuração do _plugin_ pertinente
  no arquivo pom.xml. Depende de `mvn compile`.

    coloca em execução a API gerada na porta padrão (8080).
    submetidas conforme abaixo:
    - Abra o endereço http://localhost:8080/ no seu navegador
    
## Base de dados H2 (na Memoria)
 - Base de dados fica localizado dentro da pasta resources do projeto com a identificação:
        `movielist.csv`
    Ela será inserida ao inciar a aplicação, e será removida ao fechar a aplicação, sempre 
    gerando um novo a cada inicio.
    
    
