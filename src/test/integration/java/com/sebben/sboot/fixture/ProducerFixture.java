package com.sebben.sboot.fixture;

import com.sebben.sboot.entity.Producer;
import org.apache.commons.lang3.RandomStringUtils;

public class ProducerFixture {

    private Producer producer;

    private ProducerFixture() {
    }

    public static ProducerFixture get() {
        return new ProducerFixture();
    }

    public ProducerFixture withRandomData() {
        producer = Producer
            .builder()
            .year(RandomStringUtils.randomNumeric(4))
            .winner("yes")
            .producers(RandomStringUtils.randomAlphabetic(10))
            .studios(RandomStringUtils.randomAlphabetic(10))
            .title(RandomStringUtils.randomAlphabetic(10))
            .build();
        return this;
    }

    public Producer build() {
        return producer;
    }

    public ProducerFixture withProducer(String nome) {
        producer.setProducers(nome);
        return this;
    }

}
