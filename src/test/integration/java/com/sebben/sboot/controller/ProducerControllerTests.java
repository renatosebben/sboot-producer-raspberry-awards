package com.sebben.sboot.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sebben.sboot.entity.Producer;
import com.sebben.sboot.fixture.ProducerFixture;
import com.sebben.sboot.repository.ProducerRepository;
import com.sebben.sboot.response.ProducersResponse;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestInstance(Lifecycle.PER_CLASS)
public class ProducerControllerTests {

    @Autowired
    WebApplicationContext webApplicationContext;

    @MockBean
    ProducerRepository producerRepository;

    MockMvc mockMvc;

    @BeforeAll
    public void init() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testFindProducer() throws Exception {
        Mockito.when(producerRepository.findAll()).thenReturn(getMock());

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
            .get("/v1/producer/find"))
            .andDo(print())
            .andExpect(status().isOk())
            .andReturn();

        String json = mvcResult.getResponse().getContentAsString();
        ProducersResponse value = new ObjectMapper().readValue(json, ProducersResponse.class);

        assertNotNull(value.getMax());
        assertNotNull(value.getMin());

        assertTrue((value.getMax().get(0).getInterval() > value.getMin().get(0).getInterval()));
        assertEquals(value.getMax().get(0).getInterval(), value.getMax().get(0).getFollowingWin() - value.getMax().get(0).getPreviousWin());
        assertEquals(value.getMin().get(0).getInterval(), value.getMin().get(0).getFollowingWin() - value.getMin().get(0).getPreviousWin());

        Mockito.verify(producerRepository).findAll();

    }

    public List<Producer> getMock() {
        List<Producer> list = new ArrayList<>();
        list.add(ProducerFixture.get().withRandomData().withProducer("TESTE 1").build());
        list.add(ProducerFixture.get().withRandomData().withProducer("TESTE 1").build());
        list.add(ProducerFixture.get().withRandomData().withProducer("TESTE 1").build());
        list.add(ProducerFixture.get().withRandomData().withProducer("TESTE 1").build());
        list.add(ProducerFixture.get().withRandomData().withProducer("TESTE 1").build());
        list.add(ProducerFixture.get().withRandomData().withProducer("TESTE 1").build());
        list.add(ProducerFixture.get().withRandomData().withProducer("TESTE 2").build());
        list.add(ProducerFixture.get().withRandomData().withProducer("TESTE 2").build());
        list.add(ProducerFixture.get().withRandomData().withProducer("TESTE 2").build());
        return list;
    }


}
