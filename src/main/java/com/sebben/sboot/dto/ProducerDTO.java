package com.sebben.sboot.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProducerDTO {

    private Long id;
    private String year;
    private String title;
    private String studios;
    private String producers;
    private String winner;

}
