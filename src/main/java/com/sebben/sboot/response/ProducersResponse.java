package com.sebben.sboot.response;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProducersResponse {

    private List<ProducerResponse> min;
    private List<ProducerResponse> max;

}
