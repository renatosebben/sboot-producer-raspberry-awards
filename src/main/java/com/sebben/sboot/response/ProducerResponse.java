package com.sebben.sboot.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProducerResponse implements Comparable<ProducerResponse> {

    private String producer;
    private int interval;
    private int previousWin;
    private int followingWin;

    @Override
    public int compareTo(ProducerResponse response) {
        return Integer.compare(response.getInterval(), this.interval);
    }

}
