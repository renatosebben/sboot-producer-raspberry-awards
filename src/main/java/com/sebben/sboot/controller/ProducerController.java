package com.sebben.sboot.controller;

import com.sebben.sboot.response.ProducersResponse;
import com.sebben.sboot.service.ProducerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1/producer")
@RequiredArgsConstructor
public class ProducerController {

    private final ProducerService service;

    @GetMapping("/find")
    public ResponseEntity<ProducersResponse> find() {
        return ResponseEntity.ok(service.find());
    }

}
