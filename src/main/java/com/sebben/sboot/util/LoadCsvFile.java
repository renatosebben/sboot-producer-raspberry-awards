package com.sebben.sboot.util;

import com.sebben.sboot.dto.ProducerDTO;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

@Component
public class LoadCsvFile {

    public List<ProducerDTO> producerDTOList() throws IOException {
        List<ProducerDTO> list = new ArrayList<>();
        File file = ResourceUtils.getFile("classpath:movielist.csv");

        BufferedReader fileReader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "Cp1252"));
        CSVParser csvParser = new CSVParser(fileReader, CSVFormat.DEFAULT.withDelimiter(';').withQuoteMode(QuoteMode.ALL).withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());

        Iterable<CSVRecord> csvRecords = csvParser.getRecords();

        for (CSVRecord csvRecord : csvRecords)
            if (csvRecord.size() >= csvParser.getHeaderMap().size()) {
                list.add(ProducerDTO.builder()
                    .year(csvRecord.get("year"))
                    .title(csvRecord.get("title"))
                    .studios(csvRecord.get("studios"))
                    .producers(csvRecord.get("producers"))
                    .winner(csvRecord.get("winner")).build());
            }

        return list;
    }

}
