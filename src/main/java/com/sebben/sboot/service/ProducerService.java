package com.sebben.sboot.service;

import com.sebben.sboot.dto.ProducerDTO;
import com.sebben.sboot.mapper.ProducerMapper;
import com.sebben.sboot.repository.ProducerRepository;
import com.sebben.sboot.response.ProducerResponse;
import com.sebben.sboot.response.ProducersResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
@RequiredArgsConstructor
public class ProducerService {

    private static final String AND = " and ";
    private static final String REGEX_SPLIT = ",";
    private final ProducerRepository repository;

    public ProducersResponse find() {
        var collect = repository.findAll().stream().map(ProducerMapper::toDTO)
            .collect(Collectors.toList());

        var explodeProducers = getExplodeProducers(collect);

        var listOfWinner = explodeProducers.stream().filter(bean -> bean.getWinner().equalsIgnoreCase("yes"))
            .collect(Collectors.toList());

        Map<String, List<Integer>> producerMap = new HashMap<>();
        Map<String, List<Integer>> mapOfWinners = packWinners(listOfWinner, producerMap);

        return buildResponse(mapOfWinners);
    }

    private ProducersResponse buildResponse(Map<String, List<Integer>> mapOfWinners) {
        List<ProducerResponse> list = new ArrayList<>();
        for (String producer : mapOfWinners.keySet()) {
            List<Integer> integers = mapOfWinners.get(producer);
            if (integers.size() > 1) {
                var yearOne = integers.get(0);
                var yearTwo = integers.get(integers.size() - 1);
                list.add(ProducerResponse.builder()
                    .producer(producer)
                    .previousWin(yearOne)
                    .followingWin(yearTwo)
                    .interval(yearTwo - yearOne)
                    .build());
            }
        }
        return buildProducer(list);
    }

    private ProducersResponse buildProducer(List<ProducerResponse> list) {
        if (ObjectUtils.isEmpty(list)) {
            return ProducersResponse.builder().build();
        }
        if (list.size() == 1) {
            Collections.sort(list);
            var max = getInterval(list);
            return ProducersResponse.builder()
                .max(max)
                .min(null)
                .build();
        } else {
            Collections.sort(list);
            var max = getInterval(list);
            Collections.reverse(list);
            var min = getInterval(list);
            return ProducersResponse.builder()
                .max(max)
                .min(min)
                .build();
        }
    }

    private List<ProducerResponse> getInterval(List<ProducerResponse> list) {
        List<ProducerResponse> listMax = new ArrayList<>();
        var interval = list.get(0).getInterval();
        for (ProducerResponse producerResponse : list) {
            if (producerResponse.getInterval() == interval) {
                listMax.add(producerResponse);
            } else {
                break;
            }
        }
        return listMax;
    }

    private Map<String, List<Integer>> packWinners(List<ProducerDTO> list, Map<String, List<Integer>> producerMap) {
        for (ProducerDTO dto : list) {
            if (ObjectUtils.isEmpty(producerMap.get(dto.getProducers()))) {
                List<Integer> listYear = new ArrayList<>();
                listYear.add(Integer.parseInt(dto.getYear()));
                producerMap.put(dto.getProducers(), listYear);
            } else {
                List<Integer> integers = producerMap.get(dto.getProducers());
                integers.add(Integer.parseInt(dto.getYear()));
                Collections.sort(integers);
                producerMap.replace(dto.getProducers(), integers);
            }
        }
        return producerMap;
    }

    private List<ProducerDTO> getExplodeProducers(List<ProducerDTO> list) {
        List<ProducerDTO> listExplode = new ArrayList<>();
        list.forEach(dto -> {
            if (ObjectUtils.isEmpty(dto.getProducers())) {
                return;
            }
            if (dto.getProducers().contains(AND)) {
                dto.setProducers(dto.getProducers().replace(AND, REGEX_SPLIT));
            }
            if (dto.getProducers().contains(REGEX_SPLIT)) {
                var split = dto.getProducers().split(REGEX_SPLIT);
                for (String producer : split) {
                    addExplode(listExplode, dto, producer);
                }
            } else {
                addExplode(listExplode, dto, dto.getProducers());
            }
        });
        return listExplode;
    }

    private void addExplode(List<ProducerDTO> listExplode, ProducerDTO dto, String producer) {
        listExplode.add(ProducerDTO.builder()
            .winner(dto.getWinner())
            .producers(producer.trim())
            .title(dto.getTitle())
            .studios(dto.getStudios())
            .year(dto.getYear())
            .id(dto.getId()).build());
    }


}
