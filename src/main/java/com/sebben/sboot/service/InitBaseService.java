package com.sebben.sboot.service;

import com.sebben.sboot.dto.ProducerDTO;
import com.sebben.sboot.entity.Producer;
import com.sebben.sboot.mapper.ProducerMapper;
import com.sebben.sboot.repository.ProducerRepository;
import com.sebben.sboot.util.LoadCsvFile;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class InitBaseService implements ApplicationRunner {

    private final LoadCsvFile loadCsvFile;
    private final ProducerRepository repository;

    @Override
    public void run(ApplicationArguments args) {
        try {
            log.info("Starting...");
            List<ProducerDTO> producerDTOS = loadCsvFile.producerDTOList();
            List<Producer> collect = producerDTOS.stream().map(ProducerMapper::toEntity).collect(Collectors.toList());
            repository.saveAll(collect);
            log.info("All dates save in h2 base!");
        } catch (IOException e) {
            log.error("Error in save data {}", e.getMessage());
        }
    }
}
