package com.sebben.sboot.mapper;

import com.sebben.sboot.dto.ProducerDTO;
import com.sebben.sboot.entity.Producer;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProducerMapper {

    public static Producer toEntity(ProducerDTO dto) {
        return Producer.builder()
            .id(dto.getId())
            .producers(dto.getProducers())
            .studios(dto.getStudios())
            .title(dto.getTitle())
            .winner(dto.getWinner())
            .year(dto.getYear()).build();
    }

    public static ProducerDTO toDTO(Producer dto) {
        return ProducerDTO.builder()
            .id(dto.getId())
            .producers(dto.getProducers())
            .studios(dto.getStudios())
            .title(dto.getTitle())
            .winner(dto.getWinner())
            .year(dto.getYear()).build();
    }

}
